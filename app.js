require('dotenv').config()
const express =  require('express')
const app = express()
const http = require('http')
const path = require("path")
const server = http.createServer(app)
const mongoose = require('mongoose')
const PORT = process.env.PORT || 3030
const socketio = require('socket.io')
const {verifyToken} = require("./helpers/jwt")
const io = socketio(server, {
  cors:{
    origin: process.env.URL_CLIENT,
    methods: ["GET", "POST"],
    credentials: true
  },
  allowEIO3: true
})
const cors = require("cors")
const MessageController = require("./Controllers/MessageController")
const ClientMessageController = require("./Controllers/ClientMessageController")


mongoose.connect(process.env.MONGO_DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

app.use(express.static(path.join(__dirname, 'public')))

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

server.listen(PORT, () => console.log("http://localhost:"+PORT))
let admins = []
io.on("connection", socket => {
  // console.log("Connected");

  //! ==================!!!! UNTUK KEDUANYA !!!!==================
  
  // Menyimpan pesan baru ke room chat dan mengirim notif pesan masuk
  socket.on("saveMessage", async (msg) => {
    try {
      const newMessage = await MessageController.saveMessage(msg)
      // message kirim kediri sendiri sesuai socket.id
      io.to(socket.id).emit("showNewMessage", newMessage)
      newMessage._doc.isMe = false

      // message kirim ke lawan bicara sesuai idRoom
      socket.broadcast.to(newMessage.idRoom).emit("showNewMessage", newMessage)
      io.to(newMessage._doc.toSend).emit("plusOne", newMessage.idRoom)
      
      // trigger untuk memunculkan notifikasi pesan masuk jika tidak di module messages pada admin
      io.to(newMessage._doc.toSend).emit("showNotif",  {unread: newMessage._doc.unread, send: newMessage._doc.name})

    } catch (error) {
      io.to(socket.id).emit("somethinkWrong", {})
    }
  })

    // menghapus room sebelumnya saat akan pindah ke room berikutnya
  socket.on("removeRoom", idRoom => {
    socket.leave(idRoom)
  })

  // update status menjadi sudah di baca saat message dari lawan bicara telah di kirim
  socket.on("alreadyRead", async (msg) => {
    await MessageController.doneRead(msg)
  })
  
  //! ==================!!!! UNTUK ADMIN !!!!==================
  // handle jumlah admin yang online
  socket.on("login", async (msg) => {
    try {
      let data = await verifyToken(msg.token)
      let flag = true
      if(admins.length > 0){
        for (let i = 0; i < admins.length; i++) {
          const el = admins[i];
          if(el.id === data.id){
            flag = false
          }

          if(admins.length - 1 === i && flag){
            data.socketID = socket.id
            admins.push(data)
          }
        }
      } else {
        data.socketID = socket.id
        admins.push(data)
      }
      socket.join(data.id)
      socket.broadcast.emit("adminLogin", data)
    } catch (error) {
      io.to(socket.id).emit("somethinkWrong", {})
    }
  })

  // menampilkan user yang pernah chat dengan admin
  socket.on("user", async (msg) => {
    try {
      let {tmpData} = await MessageController.showFriendChat(msg)
      
      io.to(socket.id).emit("data user", tmpData)
    } catch (error) {
      io.to(socket.id).emit("somethinkWrong", {})
    }
  })

  // menampilkan isi chat pada suatu room
  socket.on("searchRoom", async (msg) => {
    try {
      if(msg.leaveRoom){
        socket.leave(msg.leaveRoom);
      }
      socket.join(msg.roomID)
      let message = await MessageController.searchRoom(msg)
      io.to(socket.id).emit("showMessage", message)
    } catch (error) {
      io.to(socket.id).emit("somethinkWrong", {})
    }
  })

  socket.on("logout", async (id) => {
    try {
      const data = await verifyToken(id)
      socket.leave(data.id)
      const adminLeft = admins.filter(el => el.id !== data.id)
      admins = adminLeft
      socket.broadcast.emit("adminLogOut", data)
    } catch (error) {
      io.to(socket.id).emit("somethinkWrong", {})
    }
  })

  //! ==================!!!! UNTUK CLIENT !!!!==================

  socket.on("clientLogin", async (token) => {
    try {
      const client = await verifyToken(token)
      socket.join(client.id)
      io.to(socket.id).emit("adminOnline", admins)
    } catch (error) {
      io.to(socket.id).emit("somethingWrongClient",{})
    }
  })
  // checkRoom dari sisi client
  socket.on("checkRoomClient", async (msg) => {
    try {
      if(msg.roomID){
        socket.leave(msg.roomID)
      }
      const resultCheckRoom = await ClientMessageController.checkRoom(msg)

      socket.join(resultCheckRoom.roomID)

      io.to(socket.id).emit("resultCheckRoomClient", resultCheckRoom)
    } catch (error) {
      io.to(socket.id).emit("somethingWrongClient",{})
    }
  })
  
  socket.on("disconnect", () => {
    const adminOut = admins.filter(el => el.socketID === socket.id)
    socket.broadcast.emit("adminLogOut", adminOut[0])
    const adminLeft = admins.filter(el => el.socketID !== socket.id)
    admins = adminLeft
    // console.log(`User ${socket.id} is disconnect`);
  })
})