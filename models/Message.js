const mongoose = require('mongoose')

const messages = new mongoose.Schema({
  idRoom: {
    type: String,
    required: true
  },
  idSender: Number,
  // idSender: String,
  message: String,
  status: {
    type: Number,
    default: 0
  }
}, {collection: "messages", timestamps: true})

module.exports = mongoose.model("Messages", messages)