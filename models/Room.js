const mongoose = require('mongoose')
const roomSchema = new mongoose.Schema({
  room: {
    admin: Number,
    user: Number
  },
}, {collection: "rooms", timestamps: true})

module.exports = mongoose.model("Room", roomSchema)