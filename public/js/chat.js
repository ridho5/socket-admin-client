const socket = io();
const userList = document.getElementById("users")
const showMessage = document.getElementById("show-messages")
const sendMessage = document.getElementById("send-message")

socket.emit("clientLogin", localStorage.getItem("token"))

// socket.emit("checkRoomClient", { token: localStorage.getItem('token'), idAdmin: 1 })

socket.on("adminOnline", data => {
  console.log("adminOnline");
  console.log(data);
  outputUsers(data)
})

socket.on("adminLogOut", data => {
  console.log("adminLogout");
  console.log(data);
})

socket.on("adminLogin", data => {
  console.log("adminLogin");
  console.log(data);
})

socket.on("resultCheckRoomClient", msg => {
  console.log(msg);
  localStorage.setItem("room", msg.roomID)
  showMessageInDisplay(msg.messages)
})

// socket.on("showNewMessage", msg => {
//   newMessageInput(msg)
// })

// socket.on("notifOnline", (msg) => {
//   console.log(msg);
// })

function outputUsers(users){
  userList.innerHTML = `
    ${users.map(user => `
    <a href="#" class="list-group-item list-group-item-action active text-white rounded-0">
      <div part="${user.id}" class="media"><img src="https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-image-182145777.jpg" alt="user" width="50" class="rounded-circle">
        <div class="media-body ml-4">
          <div class="d-flex align-items-center justify-content-between mb-1">
            <h6 part="${user.id} class="mb-0">${user.name}</h6><small class="small font-weight-bold">25 Dec</small>
          </div>
          <p class="font-italic mb-0 text-small">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
        </div>
      </div>
    </a>
    `)}
  `
}

function showMessageInDisplay(messages){
  showMessage.innerHTML = `
  ${messages.map(message => `
    <div class="media w-50 mb-3"><img src="https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-image-182145777.jpg" alt="user" width="50" class="rounded-circle">
      <div class="media-body ml-3">
        <div class="bg-light rounded py-2 px-3 mb-2">
          <p class="text-small mb-0 text-muted">${message.message}</p>
        </div>
        <p class="small text-muted">${moment.utc(message.createdAt).local().fromNow()}</p>
      </div>
    </div>`)}
  `
}

// function newMessageInput(message){
//   showMessage.innerHTML += `
//   <div class="media w-50 mb-3"><img src="https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-image-182145777.jpg" alt="user" width="50" class="rounded-circle">
//     <div class="media-body ml-3">
//       <div class="bg-light rounded py-2 px-3 mb-2">
//         <p class="text-small mb-0 text-muted">${message.message}</p>
//       </div>
//       <p class="small text-muted">${moment.utc(message.createdAt).local().fromNow()}</p>
//     </div>
//   </div>
//   `
// }

userList.addEventListener('click', (e => {
  e.preventDefault()
  const payload = {
    idAdmin: +e.srcElement.part[0],
    token: localStorage.getItem("token"),
    limit: 10
  }

  if(localStorage.getItem("room")) {
    payload.roomID = localStorage.getItem("room")
  }
  // console.log(e.srcElement.part[0]);
  console.log(payload);
  socket.emit("checkRoomClient", payload)
}))


// sendMessage.addEventListener("submit", e => {
//   e.preventDefault()
//   const payload = {
//     idRoom: localStorage.getItem("idRoom"),
//     id: localStorage.getItem("token"),
//     message: e.target.messageToSend.value
//   }

//   e.target.messageToSend.value = ""
//   e.target.messageToSend.focus()
//   console.log(payload);
//   socket.emit("saveMessage", payload)
// })