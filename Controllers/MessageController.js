const Room = require("../models/Room")
const Message = require("../models/Message")
const axios = require("axios")
const {verifyToken} = require("../helpers/jwt")

class MessageController{
  static async showFriendChat(msg){
    try {
      // ----- verify token
      const result = await verifyToken(msg.id)
      let room
      let tmpData = []

      // ----- Mengambil list user yang sudah melakukan chatting (dari sisi admin)
      if(result.status === "admin"){
        room = await Room.find({"room.admin": (result.id)})
        for (let i = 0; i < room.length; i++) {
          const el = room[i];
          
          // ----- Mengambil data user
          // let tmpUser = await User.findById(el.room.user)
          const configUser = {
            method: "GET",
            url: process.env.URL_SERVER+"/admin/check/user/"+ el.room.user,
          }

          const {data: tmpUser} = await axios(configUser)

          // ----- Menghitung jumlah pesan yang belum di baca
          let unreadMessage = await Message.countDocuments({
            $and: [
              {idSender: el.room.user},
              {status: 0}
            ]
          })

          // ----- List user teman chat dan jumlah pesan yang belum di baca
          let user = {
            room: room[i]._id,
            _id: tmpUser.id,
            name: tmpUser.name,
            unread: unreadMessage
          }
          tmpData.push(user)
        }

      }else{
        // ----- Mengambil list user yang sudah melakukan chatting (dari sisi user)
        room = await Room.find({"room.user": (result.id)})

        // ----- Mengambil data admin
        for (let i = 0; i < room.length; i++) {
          const el = room[i];
          const config = {
            url: process.env.URL_SERVER+"/admin/check/"+ el.room.admin,
            method: "GET"
          }
          const {data: tmpUser} = await axios(config)

          // ----- Menghitung jumlah pesan yang belum di baca
          let unreadMessage = await Message.countDocuments({
            $and: [
              {idSender: el.room.admin},
              {status: 0}
            ]
          })

          // ----- List admin teman chat dan jumlah pesan yang belum di baca
          let user = {
            room: room[i]._id,
            _id: tmpUser.data.id,
            name: tmpUser.data.name,
            unread: unreadMessage
          }
          tmpData.push(user)
        }
      }
      return {tmpData, id: result.id}
    } catch (error) {
      console.log(error);
      throw error
    }
  }

  static async searchRoom(msg){
    try {
      const { roomID, id, limit } = msg

      // ----- verify token
      const token = await verifyToken(id)
      let user = {
        room: roomID,
        unread: 0
      }
      let tmpName

      // ----- Ambil semua message sesuai roomID dengan limit dikirim dari depan 
      let checkMessage = await Message.find({idRoom: roomID}).limit(limit).sort({createdAt: -1})

      // ----- Check siapa saja di dalam room
      let checkRoom = await Room.findById({_id: roomID})

      // ----- Get username untuk lawan chat & filter menghilangkan notif jumlah pesan yang belum di baca di frontend 
      let config = {
        method: "GET",
        url: process.env.URL_SERVER+"/admin/check/user/" + checkRoom.room.user
      }
      const { data: username } = await axios(config)
      tmpName = username
      // username = await User.findById({_id: checkRoom.room.user}) 
      user.name = username.name
      user._id = username.id


      // ----- Mengubah status pesan belum di baca menjadi sudah di baca (status awal 0 menjadi 1)
      for(let i = 0; i < checkMessage.length; i++){
        let el = checkMessage[i]
        if(el.idSender === checkRoom.room.user && el.status === 0){
          // ----- Mengubah status belum di baca menjadi sudah di baca
          await Message.updateOne({_id: el._id},{status:1})
        }
      }

      let message = []
      // ----- Menentukan yang mana pesan milik dari yang punya ID dan bukan
      for (let i = 0; i < checkMessage.length; i++) {
        const el = checkMessage[i];
        if(el.idSender === token.id){
          el._doc.name = token.name
          el._doc.isMe = true
        }else{
          el._doc.isMe = false
          el._doc.name = tmpName.name
        }

        message.push(el)
      }
      let output = {
        message: message.reverse(),
        user
      }
      return output
    } catch (error) {
      console.log(error);
      throw error
    }
  }

  static async saveMessage(msg){
    try {
      const { idRoom, id, message } = msg
      const token = await verifyToken(id)
      const payload = {
        idRoom, message,
        idSender: token.id
      }

      const userRoom = await Room.findById({_id: idRoom})
      
      const messages = new Message(payload)
      const newMessage = await messages.save()
      let unreadMessage
      if(token.status === "admin"){
        unreadMessage = await Message.countDocuments({
          $and: [
            {idSender: userRoom.room.user},
            {status: 0}
          ]
        })
      } else {
        unreadMessage = await Message.countDocuments({
          $and: [
            {idSender: userRoom.room.admin},
            {status: 0}
          ]
        })
      }
      newMessage._doc.unread = unreadMessage
      newMessage._doc.isMe = true
      newMessage._doc.name = token.name
      newMessage._doc.toSend = token.status === "admin" ? userRoom.room.user : userRoom.room.admin
      return newMessage
    } catch (error) {
      console.log(error);
      throw error
    }
  }
  
  static async doneRead(msg){
    try {
      await Message.updateOne({_id: msg._id},{status:1})
    } catch (error) {
      throw error
    }
  }
}

module.exports = MessageController