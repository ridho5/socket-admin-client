const Room = require("../models/Room")
const Message = require("../models/Message")
const axios = require("axios")
const {verifyToken} = require("../helpers/jwt")

class ClientMessageController{
  static async checkRoom(msg){
    try {
      const { token, idAdmin, limit } = msg
      const client = await verifyToken(token)

      let checkRoom = await Room.findOne({
        $and: [
          {"room.admin": idAdmin},
          {"room.user": client.id}
        ]
      })
      if(!checkRoom){
        const payload = {
          room: {
            admin: idAdmin,
            user: client.id
          }
        }
        const room = new Room(payload)
        const newRoom = await room.save()
        const config = {
          method: "GET",
          url: process.env.URL_SERVER+"/admin/check/" + newRoom.room.admin
        }

        const {data: tmpAdmin} = await axios(config)
        const output = {
          roomID: newRoom._id,
          dataAdmin: tmpAdmin.data,
          unread: 0,
          messages: []
        }
        return output
      } else {
        let checkMessage = await Message.find({idRoom: checkRoom._id}).limit(limit).sort({createdAt: -1})
        const config = {
          method: "GET",
          url: process.env.URL_SERVER+"/admin/check/" + checkRoom.room.admin
        }

        const {data: tmpAdmin} = await axios(config)

        for(let i = 0; i < checkMessage.length; i++){
          let el = checkMessage[i]
          if(el.idSender === checkRoom.room.admin && el.status === 0){
            // ----- Mengubah status belum di baca menjadi sudah di baca
            await Message.updateOne({_id: el._id},{status:1})
          }
        }

        let message = []
        // ----- Menentukan yang mana pesan milik dari yang punya ID dan bukan
        for (let i = 0; i < checkMessage.length; i++) {
          const el = checkMessage[i];
          if(el.idSender == client.id){
            el._doc.name = client.name
            el._doc.isMe = true
          }else{
            el._doc.isMe = false
            el._doc.name = tmpAdmin.data.name
          }
          message.push(el)
        }

        delete tmpAdmin.data.status
        const output = {
          roomID: checkRoom._id,
          dataAdmin: tmpAdmin.data,
          unread: 0,
          messages: message.reverse(),
        }
        return output
      }
    } catch (error) {
      console.log(error);
      throw error
    }
  }
}

module.exports = ClientMessageController