const jwt = require("jsonwebtoken")

async function verifyToken(token){
  try {
    let hasil = await jwt.verify(token, process.env.SECRET_JWT);
    return hasil
  } catch (error) {
    throw error
  }
}

module.exports = {verifyToken}